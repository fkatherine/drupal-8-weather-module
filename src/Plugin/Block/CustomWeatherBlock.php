<?php
/**
 * Created by PhpStorm.
 * User: kathy
 * Date: 17.07.18
 * Time: 10:43
 */


namespace Drupal\custom_weather_block\Plugin\Block;

use Drupal\Core\Block\BlockBase;
use Drupal\Core\Block\BlockPluginInterface;
use Drupal\Core\Form\FormStateInterface;
use Drupal\custom_weather_block\Service\WeatherJsonService;

/**
 * Provides a block with a simple text.
 *
 * @Block(
 *   id = "custom_weather_block",
 *   admin_label = @Translation("custom_weather_block"),
 * )
 *
 */
class CustomWeatherBlock extends BlockBase implements BlockPluginInterface {

	public function defaultConfiguration() {
		return [
			'temperature_units' => 'metric',
			'city'              => 703448, //Kiev
			'api_key'           => '40fd2bf77902ca3604e2a98d86e45939'
		];
	}

	public function blockForm($form, FormStateInterface $form_state) {
		$cities                    = new WeatherJsonService();
		$form['api_key']           = [
			'#type'          => 'textfield',
			'#title'         => $this->t('API Key'),
			'#default_value' => $this->configuration['api_key'],
		];
		$form['temperature_units'] = [
			'#type'          => 'radios',
			'#title'         => $this->t('Temperature units'),
			'#options'       => [
				'metric'   => 'C',
				'imperial' => 'F'
			],
			'#default_value' => $this->configuration['temperature_units'],
		];
		$form['city']              = [
			'#type'          => 'select',
			'#title'         => $this->t('City (UA)'),
			'#options'       => $cities->getCities(),
			'#default_value' => $this->configuration['city'],
		];

		return $form;
	}

	public function blockSubmit($form, FormStateInterface $form_state) {
		$this->configuration['temperature_units'] = $form_state->getValue('temperature_units');
		$this->configuration['city']              = $form_state->getValue('city');
		$this->configuration['api_key']           = $form_state->getValue('api_key');
	}

	public function build() {
		$weather_service = new WeatherJsonService();
		$ans             = $weather_service->getWeather($this->configuration['api_key'], $this->configuration['city'], $this->configuration['temperature_units']);

		return [
			'#theme' => 'custom_weather_block',
			'#city'  => $ans->name,
			'#temp'  => $ans->main->temp,
			'#units' => $this->configuration['temperature_units'] == 'metric' ? 'C' : 'F',
			'#press' => $ans->main->pressure,
			'#humid' => $ans->main->humidity,
			'#img'   => $ans->weather[0],
		];
	}
}
