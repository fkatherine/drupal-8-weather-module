<?php
/**
 * Created by PhpStorm.
 * User: kathy
 * Date: 17.07.18
 * Time: 14:03
 */

namespace Drupal\custom_weather_block\Service;

use Drupal\custom_weather_block;


class WeatherJsonService {

	protected $api_url = "http://api.openweathermap.org/data/2.5/weather";
	private $cities;

	/**
	 * WeatherJsonService constructor.
	 */
	public function __construct() {
		$this->cities = $this->loadCities();
	}

	/**
	 * parse cities from Json file
	 * @return array
	 */
	private function loadCities() {
		$file_content = file_get_contents(DRUPAL_ROOT . "/modules/custom_weather_block/cities.json");
		$string       = trim(preg_replace('/\n/', ' ', $file_content));
		$json_arr     = json_decode($string);

		return $this->buildCityArray($json_arr);
	}

	private function buildCityArray($json_arr) {
		$result = [];
		foreach ($json_arr as $key => $val) {
			$result[$val->id] = $val->name;
		}

		return $result;
	}

	/**
	 * @return string
	 */
	public function getApiUrl() {
		return $this->api_url;
	}

	public function getCities() {
		return $this->cities;
	}

	/**
	 * run api request
	 *
	 * @param string $api_key
	 * @param int $city_id
	 * @param string $temp_units
	 *
	 * @return bool|mixed
	 */
	public function getWeather($api_key = "", $city_id = 0, $temp_units = "metric") {
		$ch  = curl_init();
		$url = $this->api_url . "?appid=" . $api_key . "&id=" . $city_id . "&units=" . $temp_units;

		curl_setopt($ch, CURLOPT_URL, $url);
		curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);

		$result = curl_exec($ch);

		curl_close($ch);

		if ($result) {
			return json_decode($result);
		} else {
			return false;
		}
	}


}